<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="author" content="Bradley Oosterveen">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Slabo+27px" rel="stylesheet">
    <title>Homepage</title>
    <script src="./frameworks/http.js"></script>
    <script src="./frameworks/cookie.js"></script>

    <script>
        request = {
            search: () => {
            window.location.search=http.queryEncode({tags: document.getElementById('search').value.split(' '), page: 1});
        },

        getPageQuery: (operation = 1) => {
            var q = http.queryDecode(window.location.search);
            q['page'] += operation;
            return './' + http.queryEncode(q);
        },
        }

        window.onload = () => {
            //initialize search bar;
            // 13 = enter
            document.getElementById('search').onkeypress = (e) => {
                if (e.keyCode == 13) {
                    request.search();
                }
            }
            if (window.location.search) {
                var tags = http.queryDecode(window.location.search)['tags'];
                if (tags != "undefined") {
                    document.getElementById('search').value = tags.join(' ');
                }
            }
            //initialize navigation-paging
            var e;
            if (e = document.querySelectorAll('.page-skip-1-negative')[0]) {
                e.href = request.getPageQuery(-1);
            }
            if (e = document.querySelectorAll('.page-skip-1-positive')[0]) {
                e.href = request.getPageQuery(1);
            }
            if (e = document.querySelectorAll('.page-skip-5-negative')[0]) {
                e.href = request.getPageQuery(-5);
            }
            if (e = document.querySelectorAll('.page-skip-5-positive')[0]) {
                e.href = request.getPageQuery(5);
            }
        }
    </script>
</head>
<body>
<nav>
    <div class="nav_container">
        <div class="nav_s_left">
            <div class="s_l_p_left">
                <a href="index.php" class="n_l_active">Thuispagina</a>
            </div>
            <div class="s_l_p_right">
                <a href="#">Weetje</a>
            </div>
        </div>
        <div class="search_bar">
            <input type="text" id="search" placeholder="Search" onkeypress="alert();">
        </div>
        <div class="nav_s_right">
            <div class="dropdown">
                <div class="s_l_p_right">
                    <a href="#">Account</a>
                </div>
                <div class="account_dropdown">
                    <a href="#">Profiel</a>
                    <a href="Login.php">Inloggen</a>
                    <a href="Registratie.php">Account aanmaken</a>
                    <a href="#">Instellingen</a>
                </div>
            </div>
        </div>
    </div>
</nav>
<header>
    <h1>Weetje van de dag.</h1>
    <?php
        // Include the connection to database
        require 'php/dbconnect.php';

        if ($pdo) {
            $random = "
                SELECT facts.ID , `facts`.`fact`,`user`.`username`,`facts`.`date` 
                FROM `facts`
                INNER JOIN `user` ON `user`.`ID`  = `facts`.`ID` 
                ORDER BY RAND() 
                LIMIT 1 " ;
            $random = $pdo->prepare($random);
            $random->execute();

            $row = $random->fetchAll();
            if($row){
                foreach ($row as $e => $r) {
                    echo "<div class='fact'>";
                    echo "<div class='fact_content'>";
                    echo $r['fact'];
                    echo "</div>";
                    break;
                }
            }
        }
        ?>
    <div class="header_bottom">
        <p class="h_p_left"><?php  echo "Posted on: ".$r['date']."&nbsp;|&nbsp;"; ?></p>
        <p class="h_p_right"><?php echo "by: <a href='".$_SERVER['PHP_SELF']."?target=".$r['username']."'>".$r['username']."</a>"; ?></p>
    </div>
</header>
<main>
    <!-- Search Results -->
    <div class="search_container">
    <?php
    //Search-results
    if (isset($_GET['tags']) && isset($_GET['page'])) {
        $tags = array_filter(json_decode($_GET['tags']), function($value) {return $value !== '';});
        sort($tags);

        $conn = pdo_connect('localhost', 'knowitall', 'kia_admin', 'kia_password');
        if ($conn) {
            $q = "SELECT `ID`, `tags`, `fact`, (";
            foreach ($tags as $k0 => $v0) {
                $q .= "JSON_CONTAINS(tags, '\"" . (strtolower($v0)) . "\"')";
                if (count($tags) - 1 == $k0) {
                    break;
                }
                $q .= " + ";
            }
            $q .= ") AS 'results' FROM `facts` HAVING results > 0 ORDER BY results DESC;";
            $q = $conn->prepare($q);
            $q->execute();

            $row = $q->fetchAll();
            $totalResults = count($row);
            echo '<br>';
            $row = array_slice($row, 10 * ($_GET['page'] - 1), 10);
            if (count($row) > 0) {
                echo '<br>';
                foreach ($row as $k0 => $v0) {
                    echo 'ID: ' . $v0['ID'] . ', tags: ';
                    $dt = json_decode($v0['tags']);
                    foreach ($dt as $k1 => $v1) {
                        echo $v1;
                        if (count($dt) - 1 == $k1) {
                            break 1;
                        }
                        echo ', ';
                    }
                    echo '<br>' . ($v0['fact']) . '<br><br>';
                }
                echo 'Totaal resultaten: ' . $totalResults . '<br><br>';
                //Navigation-paging
                ?><div class="navigation-paging"><?php
                if ($_GET['page'] > 5) {
                    echo '<a href="./" class="page-number page-skip-5-negative">-5</a>';
                }
                if ($_GET['page'] > 1) {
                    echo '<a href="./" class="page-number page-skip-1-negative">-1</a>';
                }
                echo '<span class="page-number page-current"> ' . $_GET['page'] . ' </span>';
                if (($_GET['page'] * 10) % $totalResults > 0 && ($_GET['page'] * 10) - $totalResults <= 0) {
                    echo '<a href="./" class="page-number page-skip-1-positive">+1</a>';
                }
                if (($_GET['page'] * 50) % $totalResults > 0 && (($_GET['page'] + 4) * 10) - $totalResults <= 0) {
                    echo '<a href="./" class="page-number page-skip-5-positive">+5</a>';
                }
                ?></div><?php
            } else {
                echo '<br>Geen resultaten...';
            }
        }
    }
    ?>
    </div>
    <!-- Facts -->
    <div class="main_container">
        <div class="main_header">
            <select name="sort" onchange="location=this.value;">
                <option value="none">Sort by: </option>
                <option value="<?php echo $_SERVER['PHP_SELF'].'?sort=new';?>">New</option>
                <option value="<?php echo $_SERVER['PHP_SELF'].'?sort=old';?>">Old</option>
                <option value="<?php echo $_SERVER['PHP_SELF'].'?sort=date';?>">Placeholder</option>
            </select>
            <?php
            if(isset($_GET["sort"])){
                switch($_GET["sort"]){
                    case "new":
                        echo "New";
                        break;
                    case "old":
                        echo "Old";
                        break;
                    case "date":
                        echo "Date";
                        break;
                }
            }else{
                echo "New";
            }
            ?>
        </div>
        <?php
        $conn = pdo_connect('localhost', 'knowitall', 'kia_admin', 'kia_password');
        $maxposts = 0;
        if($conn){
            $q = "";
            if(isset($_GET["sort"])){
                switch($_GET["sort"]){
                    case "new": // from new to old
                        $q = "
                        SELECT `facts`.`ID`,`facts`.`user_ID`, `facts`.`fact`, DATE_FORMAT(`facts`.`date`, '%d %M, %Y at %T'), `user`.`username`, `user`.`ID` 
                        FROM `facts` 
                        INNER JOIN `user` ON `facts`.`user_ID` = `user`.`ID` 
                        ORDER BY `date` DESC
                        ";
                        break;
                    case "old": // from old to new
                        $q = "
                        SELECT `facts`.`ID`,`facts`.`user_ID`, `facts`.`fact`, DATE_FORMAT(`facts`.`date`, '%d %M, %Y at %T'), `user`.`username`, `user`.`ID` 
                        FROM `facts` 
                        INNER JOIN `user` ON `facts`.`user_ID` = `user`.`ID` 
                        ORDER BY `date` ASC
                        ";
                        break;
                    case "date": // show posts on date
                        $q = "
                        SELECT `facts`.`ID`,`facts`.`user_ID`, `facts`.`fact`, DATE_FORMAT(`facts`.`date`, '%d %M, %Y at %T'), `user`.`username`, `user`.`ID` 
                        FROM `facts`
                        INNER JOIN `user` ON `facts`.`user_ID` = `user`.`ID` 
                        ORDER BY `date` ASC
                        ";
                        break;
                }
                $q = $conn->prepare($q);
                $q->execute();

                $row = $q->fetchAll();
                if($row){
                    foreach($row as $a0 => $b0){
                        echo "<div class='fact'>";
                        echo "<div class='fact_content'>";
                        echo $b0['fact'];
                        echo "</div>";
                        echo "<div class='fact_date'>";
                        echo "Posted on: ".$b0[3]."&nbsp;|&nbsp;";
                        echo "</div>";
                        echo "<div class='fact_user'>";
                        echo "by: <a href='".$_SERVER['PHP_SELF']."?target=".$b0['username']."'>".$b0['username']."</a>";
                        echo "</div>";
                        echo "</div>";
                        if($maxposts++ == 10)break;
                    }
                }
            }else{ // default sort
                $q = "
                SELECT `facts`.`ID`,`facts`.`user_ID`, `facts`.`fact`, `facts`.`date`, `user`.`username`, `user`.`ID` 
                FROM `facts` 
                INNER JOIN `user` ON `facts`.`user_ID` = `user`.`ID` 
                ORDER BY `date` DESC
                ";
                $q = $conn->prepare($q);
                $q->execute();

                $row = $q->fetchAll();
                if($row){
                    foreach($row as $a0 => $b0){
                        echo "<div class='fact'>";
                        echo "<div class='fact_content'>";
                        echo $b0['fact'];
                        echo "</div>";
                        echo "<div class='fact_date'>";
                        echo "Posted on: ".$b0['date']."&nbsp;|&nbsp;";
                        echo "</div>";
                        echo "<div class='fact_user'>";
                        echo "by: <a href='".$_SERVER['PHP_SELF']."?target=".$b0['username']."'>".$b0['username']."</a>";
                        echo "</div>";
                        echo "</div>";
                        if($maxposts++ == 10)break;
                    }
                }
            }
        }
        ?>
    </div>
</main>
<footer>
    <div class="footer_container">
        <div class="footer_s_left">
            <a href="#">Over ons</a>
            <a href="#">Contact</a>
            <a href="#">Help</a>
            <a href="#">Voorwaarden</a>
        </div>
        <img class="footer_s_logo" src="img/logo.png">
        <div class="footer_s_right">
            <a href="#">Facebook</a>
            <a href="#">Twitter</a>
            <a href="#">Instagram</a>
            <a href="#">RSS</a>
        </div>
    </div>
    <hr>
</footer>
</body>
</html>

<?php
//Functions:
function pdo_connect($s, $d, $u, $p, $o = []) {
    try {
        $c = new PDO('mysql:host=' . ($s) . ';dbname=' . ($d) . '', $u, $p, $o);
    }
    catch(PDOException $e)
    {
        echo "Connection failed: " . $e->getMessage();
        die();
    }
    return $c;
}
?>