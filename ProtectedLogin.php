<?php
// start de sessie
session_start();

// controleer of je bent ingelogd
if(!isset($_SESSION['user_username']) || !isset($_SESSION['logged_in'])){
    //User not logged in. Terugsturen naar hoofd pagina.
    header('Location: index.php');
    exit;
}
//echo 'Congratulations! You are logged in!';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="author" content="Andy de Jong">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Slabo+27px" rel="stylesheet">
    <title>Logged in</title>
</head>
<body>

<nav>
    <div class="nav_container">
        <div class="nav_s_left">
            <div class="s_l_p_left">
                <a href="#" class="n_l_active">Thuispagina</a>
            </div>
            <div class="s_l_p_right">
                <a href="#">Weetje</a>
            </div>
        </div>
        <div class="search_bar">
            <input type="text" name="search_bar" placeholder="Search">
        </div>
        <div class="nav_s_right">
            <div class="dropdown">
                <div class="s_l_p_right">
                    <a href="#">Account</a>
                </div>
                <div class="account_dropdown">
                    <a href="#">Profiel</a>
                    <a href="php/logout.php">Uitloggen</a>
                    <a href="./instellingen.php">Instellingen</a>
                </div>
            </div>
        </div>
    </div>
</nav>
<header>
        <h1>Random weetje.</h1>
            <h2><?php
            // Include the connection to database
            require 'php/dbconnect.php';

            if ($pdo) {
                $random = "
                SELECT facts.ID , `facts`.`fact`,`user`.`username`,`facts`.`date` 
                FROM `facts`
                INNER JOIN `user` ON `user`.`ID`  = `facts`.`ID` 
                ORDER BY RAND() 
                LIMIT 1 " ;
                $random = $pdo->prepare($random);
                $random->execute();

                $row = $random->fetchAll();
                if($row){
                    foreach ($row as $e => $r) {
                        echo "<div class='fact'>";
                        echo "<div class='fact_content'>";
                        echo $r['fact'];
                        echo "</div>";
                        echo "<div class='fact_date'>";
                        echo "Posted on: ".$r['date']."&nbsp;|&nbsp;";
                        echo "</div>";
                        echo "<div class='fact_user'>";
                        echo "by: <a href='".$_SERVER['PHP_SELF']."?target=".$r['username']."'>".$r['username']."</a>";
                        echo "</div>";
                        echo "</div>";
                        break;
                    }
                }
            }
                ?></h2>
        </div>
    </div>
</header>
<main>

</main>
<footer>
    <div class="footer_container">
        <div class="footer_s_left">
            <a href="#">Over ons</a>
            <a href="#">Contact</a>
            <a href="#">Help</a>
            <a href="#">Voorwaarden</a>
        </div>
        <img class="footer_s_logo" src="img/logo.png">
        <div class="footer_s_right">
            <a href="#">Facebook</a>
            <a href="#">Twitter</a>
            <a href="#">Instagram</a>
            <a href="#">RSS</a>
        </div>
    </div>
    <hr>
</footer>
</body>
</html>