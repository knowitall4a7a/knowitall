<?php
// verbindingsgegevens voor de database
//Our MySQL user account.
define('MYSQL_USER', 'kia_admin');

//Our MySQL password.
define('MYSQL_PASSWORD', 'kia_password');

//The server that MySQL is located on.
define('MYSQL_HOST', 'localhost');

//The name of our database.
define('MYSQL_DATABASE', 'knowitall');

// verbinding maken met de database
$pdo = new PDO(
    "mysql:host=" . MYSQL_HOST .
    ";dbname=" . MYSQL_DATABASE, //DSN
    MYSQL_USER, //Username
    MYSQL_PASSWORD //Password
);

?>