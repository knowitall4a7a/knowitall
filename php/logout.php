<?php
session_start();

if(isset($_SESSION['user_username'])) {
    session_destroy();
    unset($_SESSION['user_username']);
    unset($_SESSION['logged_in']);
    header("Location: ../index.php");
} else {
    header("Location: ../index.php");
}
?>