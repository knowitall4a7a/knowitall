<?php
session_start();
if (isset($_POST['request'])) {
    switch ($_POST['request']) {
        case 'ban':
            if ($_SESSION['user_authentication'] >= 2 && isset($_POST['username'])) {
                try {
                    $pdo = new PDO('mysql:host=localhost;dbname=knowitall', 'kia_admin', 'kia_password');
                    $query = $pdo->prepare("SELECT `ID`, `authentication` FROM `user` WHERE `username` = ?");
                    $query->bindValue(1, $_POST['username']);
                    if ($query->execute()) {
                        if ($query->rowCount() == 1) {
                            $rows = $query->fetchAll(PDO::FETCH_CLASS);
                            if (strlen($rows[0]->authentication) == 1) {
                                if ($rows[0]->authentication == 0) {
                                    die(json_encode(array('response' => 'ban', 'success' => 2, 'username' => $_POST['username'])));
                                }
                                if ($rows[0]->authentication > 1) {
                                    die(json_encode(array('response' => 'ban', 'success' => 3, 'username' => $_POST['username'])));
                                }
                            } else {
                                die(json_encode(array('response' => 'ban', 'success' => 4, 'username' => $_POST['username'])));
                            }
                        } else {
                            die(json_encode(array('response' => 'ban', 'success' => 0, 'username' => $_POST['username'])));
                        }
                    } else {
                        var_dump($query->errorInfo());
                        die();
                    }

                    $query = $pdo->prepare("UPDATE `user` SET `authentication` = 0 WHERE `username` = ?");
                    $query->bindValue(1, $_POST['username']);
                    if ($query->execute()) {
                        if ($query->rowCount() == 1) {
                            echo json_encode(array('response' => 'ban', 'success' => 1, 'username' => $_POST['username']));
                        }
                    } else {
                        var_dump($query->errorInfo());
                        die();
                    }
                } catch (PDOException $error) {
                    die($error->getMessage());
                }
            }
            break;
        case 'unban':
            if ($_SESSION['user_authentication'] >= 2 && isset($_POST['username'])) {
                try {
                    $pdo = new PDO('mysql:host=localhost;dbname=knowitall', 'kia_admin', 'kia_password');
                    $query = $pdo->prepare("SELECT `ID`, `authentication` FROM `user` WHERE `username` = ?");
                    $query->bindValue(1, $_POST['username']);
                    if ($query->execute()) {
                        if ($query->rowCount() == 1) {
                            $rows = $query->fetchAll(PDO::FETCH_CLASS);
                            if (strlen($rows[0]->authentication) == 1) {
                                if ($rows[0]->authentication > 0) {
                                    die(json_encode(array('response' => 'unban', 'success' => 2, 'username' => $_POST['username'])));
                                }
                            } else {
                                die(json_encode(array('response' => 'ban', 'success' => 4, 'username' => $_POST['username'])));
                            }
                        } else {
                            die(json_encode(array('response' => 'unban', 'success' => 0, 'username' => $_POST['username'])));
                        }
                    } else {
                        var_dump($query->errorInfo());
                        die();
                    }

                    $query = $pdo->prepare("UPDATE `user` SET `authentication` = 1 WHERE `username` = ?");
                    $query->bindValue(1, $_POST['username']);
                    if ($query->execute()) {
                        if ($query->rowCount() == 1) {
                            echo json_encode(array('response' => 'unban', 'success' => 1, 'username' => $_POST['username']));
                        }
                    } else {
                        var_dump($query->errorInfo());
                        die();
                    }
                } catch (PDOException $error) {
                    die($error->getMessage());
                }
            }
            break;
        case 'promote':
            if ($_SESSION['user_authentication'] == 3 && isset($_POST['username'])) {
                try {
                    $pdo = new PDO('mysql:host=localhost;dbname=knowitall', 'kia_admin', 'kia_password');
                    $query = $pdo->prepare("SELECT `ID`, `authentication` FROM `user` WHERE `username` = ?");
                    $query->bindValue(1, $_POST['username']);
                    if ($query->execute()) {
                        if ($query->rowCount() == 1) {
                            $rows = $query->fetchAll(PDO::FETCH_CLASS);
                            if (strlen($rows[0]->authentication) == 1) {
                                if ($rows[0]->authentication == 0) {
                                    die(json_encode(array('response' => 'promote', 'success' => 3, 'username' => $_POST['username'])));
                                }
                                if ($rows[0]->authentication >= 2) {
                                    die(json_encode(array('response' => 'promote', 'success' => 2, 'username' => $_POST['username'])));
                                }
                            } else {
                                die(json_encode(array('response' => 'unban', 'success' => 4, 'username' => $_POST['username'])));
                            }
                        } else {
                            die(json_encode(array('response' => 'promote', 'success' => 0, 'username' => $_POST['username'])));
                        }
                    } else {
                        var_dump($query->errorInfo());
                        die();
                    }

                    $query = $pdo->prepare("UPDATE `user` SET `authentication` = 2 WHERE `username` = ?");
                    $query->bindValue(1, $_POST['username']);
                    if ($query->execute()) {
                        if ($query->rowCount() == 1) {
                            echo json_encode(array('response' => 'promote', 'success' => 1, 'username' => $_POST['username']));
                        }
                    } else {
                        var_dump($query->errorInfo());
                        die();
                    }
                } catch (PDOException $error) {
                    die($error->getMessage());
                }
            }
            break;
        case 'demote';
            if ($_SESSION['user_authentication'] == 3 && isset($_POST['username'])) {
                try {
                    $pdo = new PDO('mysql:host=localhost;dbname=knowitall', 'kia_admin', 'kia_password');
                    $query = $pdo->prepare("SELECT `ID`, `authentication` FROM `user` WHERE `username` = ?");
                    $query->bindValue(1, $_POST['username']);
                    if ($query->execute()) {
                        if ($query->rowCount() == 1) {
                            $rows = $query->fetchAll(PDO::FETCH_CLASS);
                            if (strlen($rows[0]->authentication) == 1) {
                                if ($rows[0]->authentication < 2) {
                                    die(json_encode(array('response' => 'demote', 'success' => 2, 'username' => $_POST['username'])));
                                }
                                if ($rows[0]->authentication > 2) {
                                    die(json_encode(array('response' => 'demote', 'success' => 3, 'username' => $_POST['username'])));
                                }
                            } else {
                                die(json_encode(array('response' => 'unban', 'success' => 4, 'username' => $_POST['username'])));
                            }
                        } else {
                            die(json_encode(array('response' => 'demote', 'success' => 0, 'username' => $_POST['username'])));
                        }
                    } else {
                        var_dump($query->errorInfo());
                        die();
                    }

                    $query = $pdo->prepare("UPDATE `user` SET `authentication` = 1 WHERE `username` = ?");
                    $query->bindValue(1, $_POST['username']);
                    if ($query->execute()) {
                        if ($query->rowCount() == 1) {
                            echo json_encode(array('response' => 'demote', 'success' => 1, 'username' => $_POST['username']));
                        }
                    } else {
                        var_dump($query->errorInfo());
                        die();
                    }
                } catch (PDOException $error) {
                    die($error->getMessage());
                }
            }
            break;
        case 'anonymize':
            try {
                $pdo = new PDO('mysql:host=localhost;dbname=knowitall', 'kia_admin', 'kia_password');
                $query = $pdo->prepare("UPDATE `settings` SET `value` = ? WHERE variable = 'anonymous_names';");
                $query->bindValue(1, $_POST['anonymize']);
                if ($query->execute()) {
                    echo json_encode(array('response' => 'anonymize', 'success' => intval($_POST['anonymize']) + 1));
                } else {
                    echo json_encode(array('response' => 'demote', 'success' => 0, 'error' => $query->errorInfo()[2]));
                    die();
                }
            } catch (PDOException $error) {
                die($error->getMessage());
            }
            break;
        case 'anonymous_names_setting':
            try {
                $pdo = new PDO('mysql:host=localhost;dbname=knowitall', 'kia_admin', 'kia_password');
                $query = $pdo->prepare("SELECT `value` FROM `settings` WHERE variable = 'anonymous_names';");
                if ($query->execute()) {
                    if ($query->rowCount() == 1) {
                        $rows = $query->fetchAll(PDO::FETCH_CLASS);
                        echo json_encode(array('response' => 'anonymize', 'success' => 1, 'anonymous_names_setting' => $rows[0]->value));
                    } else {
                        die(json_encode(array('response' => 'anonymize', 'success' => 2)));
                    }
                } else {
                    echo json_encode(array('response' => 'demote', 'success' => 0, 'error' => json_encode($query->errorInfo())));
                    die();
                }
            } catch (PDOException $error) {
                die($error->getMessage());
            }
            break;
    }
}
