<?php
// start de sessie
session_start();

require 'php/dbconnect.php';

if(isset($_POST['login'])){

    //Retrieve the field values from our login form.
    $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
    $passwordAttempt = !empty($_POST['password']) ? trim($_POST['password']) : null;

    //Beveilig de wachtwoorden.
    $passwordHash = hash("sha256", $passwordAttempt);

    //Haal de useraccount info op..
    $sql = "SELECT `username`, `password`, `authentication` FROM `user` WHERE username = :username";
    $stmt = $pdo->prepare($sql);

    //Bind de username aan de parameter.
    $stmt->bindValue(':username', $username);

    //Execute.
    $stmt->execute();

    //Fetch row.
    $username = $stmt->fetch(PDO::FETCH_ASSOC);

    //If $row is FALSE.
    if($username === false){
        //User bestaat nog niet !
        die('Incorrect username / password combination!');
    } else{
        //Account gevonden controleer het password in het systeem.

        //Vergelijk de passworden.
        $validPassword = $passwordHash == $username['password'] ;

        //Als $validPassword gelijk is aan TRUE, de login is succesvol.
        if($validPassword){

            //Maak een login session voor de user.
            $_SESSION['user_username'] = $username['username'];
            $_SESSION['logged_in'] = time();
            $_SESSION['user_authentication'] = $username['authentication'];

            //Naar de beveiligde pagina
            header('Location: ProtectedLogin.php');

            exit;

        } else{
            //$validPassword was FALSE. Passwords komen niet overheen.
            header('Location: Login.php?signup=error');
            exit();

        }
    }

}

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<form action="Login.php" method="post">
    Username:<br />
    <input type="text" name="username" value="" required />
    <br /><br />
    Password:<br />
    <input type="password" name="password" value="" required />
    <br /><br />
    <button type="submit" class="submit" name="login" >Log In</button>
</form>

<?php

$fullurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if (strpos($fullurl, "signup=error") == true) {
    echo "<P class='error'><br>Password en user komen niet overeen !</P>";
    exit();
}
?>
</body>
</html>
