cookie = {
	set: (name, value, date = new Date(2147483647000), add = 0, path = '', domain = '', secure = false) => {
		date.setTime(date.getTime() + add);
		document.cookie = name + '=' + encodeURIComponent(JSON.stringify(value)) + ';expires=' + (date.toUTCString()) + ';path=' + path + ';domain=' + domain + (secure ? c += ';secure' : '');
	},
	delete: (name, path = '') => {
		document.cookie = name + '=""; max-age=0; path=' + path;
	},
	clear: () => {
		var c = cookie.parse();
		for (i in c) {
			cookie.delete(i);
		}
	},
	parse: () => {
		var c = document.cookie.split('; ');
		for (i in c) {
			c[i] = c[i].split('=');
		}
		var o = {};
		for (i in c) {
			if (c[i][1] != undefined) {
				o[c[i][0]] = JSON.parse(decodeURIComponent(c[i][1]));
			}
		} 
		return (o);
	},
	read: (name) => {
		var cookie = cookie.parse();
		if (typeof cookie[name] != 'undefined') {
			return cookie[name];
		}
		return undefined;
	}
}