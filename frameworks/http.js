window.addEventListener('load', HTTPinit);

function HTTPinit() {
	window.http = {
		request: (url, response = () => { }, fd = undefined, qs = '', open = false) => {
			if (!open) {
				if (typeof fd == 'object') {
					var data = http.form(fd);
				}
				var q = '';
				if (typeof qs == 'object') {
					q = http.queryEncode(qs);
				}

				req = new XMLHttpRequest();

				req.onreadystatechange = function (t) {
					if (this.status == 200 && this.readyState == 4) {
						var packet;
						try {
							packet = JSON.parse(this.responseText);
						} catch (error) {
							packet = this.responseText;
						}
						response(packet, this);
					}
				}
				req.open('POST', url + q, true);
				req.send(data);
			} else {
				var q = '';
				if (typeof qs == 'object') {
					q = http.queryEncode(qs);
				}

				var f = document.createElement('form');
				f.action = url + q;
				f.method = 'post';
				f.name = 'form';
				f.enctype = 'multipart/form-data';

				if (fd.length == undefined) {
					for (var i in fd) {
						var e = document.createElement('input');
						e.type = 'hidden';
						e.name = i;
						e.value = (typeof fd[i] == 'object' ? JSON.stringify(fd[i]) : fd[i]);
						f.appendChild(e);
					}
				} else {
					for (var i in fd) {
						if (fd[i][1].constructor !== HTMLInputElement) {
							var e = document.createElement('input');
							e.type = 'hidden';
							e.name = fd[i][0];
							e.value = (typeof fd[i][1] == 'object' ? JSON.stringify(fd[i][1]) : fd[i][1]);
							f.appendChild(e);
						} else {
							var e = document.createElement('input');
							e.type = 'file';
							e.name = fd[i][0];
							e.files = fd[i][1].files;
							f.appendChild(e);
						}
					}
				}

				document.body.appendChild(f);
				f.submit();
			}
		},
		urlDecode: (url) => {
			return decodeURIComponent(url.replace(/\+/g, ' '));
		},
		urlEncode: (url) => {
			return encodeURIComponent(url).replace(/%20/g, '+');
		},
		queryEncode: (qs) => {
			var s = '?';
			if (qs.length == undefined) {
				for (i in qs) {
					if (typeof qs[i] == 'object') {
						s += i + '=' + http.urlEncode(JSON.stringify(qs[i])) + '&';
					} else {
						s += i + '=' + http.urlEncode(qs[i]) + '&';
					}
				}
			} else {
				for (i in qs) {
					if (typeof qs[i][1] == 'object') {
						s += qs[i][0] + '=' + http.urlEncode(JSON.stringify(qs[i][1])) + '&';
					} else {
						s += qs[i][0] + '=' + http.urlEncode(qs[i][1]) + '&';
					}
				}
			}
			return s.substring(0, s.length - 1);
		},
		queryDecode: (str, obj = true) => {
			var qs = str.substring(1, str.length).split('&');
			if (obj) {
				var o = {};
				for (i in qs) {
					var a = qs[i].split('=');
					var d;
					try {
						d = JSON.parse(http.urlDecode(a[1]));
					} catch (error) {
						d = http.urlDecode(a[1]);
					}
					o[a[0]] = d;
				}
			} else {
				var o = [];
				for (i in qs) {
					o.push(qs[i].split('='));
					var d;
					try {
						d = JSON.parse(http.urlDecode(o[i][1]));
					} catch (error) {
						d = http.urlDecode(o[i][1]);
					}
					o[i][1] = d;
				}
			}
			return o;
		},
		form: (fd) => {
			var d = new FormData();
			if (fd.length == undefined) {
				for (var i in fd) {
					d.append(i, (typeof fd[i] == 'object' ? JSON.stringify(fd[i]) : fd[i]));
				}
			} else {
				for (var i in fd) {
					if (fd[i].length == 2) {
						d.append(fd[i][0], (typeof fd[i][1] == 'object' ? JSON.stringify(fd[i][1]) : fd[i][1]));
					} else {
						d.append(fd[i][0], fd[i][1], fd[i][2]);
					}
				}
			}
			return d;
		},
		upload: (ele, name = 'upload') => {
			var f = [];
			var e = ele.files;
			for (var i = 0; i < e.length; i++) {
				f.push([name + '[]', e[i], e[i].name]);
			}
			return f;
		}
	}
}