<?php
// start the sessie
session_start();

// Include the connection to database
require 'php/dbconnect.php';

//Check of de button is geklikt
if(isset($_POST['submit'])) {

    //Haal de waarden van de form.
    $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
    $firstname = !empty($_POST['firstname']) ? trim($_POST['firstname']) : null;
    $lastname = !empty($_POST['lastname']) ? trim($_POST['lastname']) : null;
    $email = !empty($_POST['email']) ? trim($_POST['email']) : null;
    $tel = !empty($_POST['tel']) ? trim($_POST['tel']) : null;
    $pass = !empty($_POST['pass']) ? trim($_POST['pass']) : null;

    // Kijk of de invoer waarden goed zijn.
    if (!preg_match("/^[a-zA-Z ]*$/", $firstname) || !preg_match("/^[a-zA-Z ]*$/", $lastname)) {
        header("Location: Registratie.php?signup=falsevalue");
        exit();
    } else {
        // Kijk of de email goed is.
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            exit();
        }

        //Kijk of de username al bestaat.

        //Sql statement maken
        $sql = "SELECT COUNT(username) AS num FROM `user` WHERE username = :username";
        $stmt = $pdo->prepare($sql);

        //Bind de username aan de parameter.
        $stmt->bindValue(':username', $username);

        //Execute.
        $stmt->execute();

        //Fetch the row.
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        //Als de username al bestaat laat een error zien.

        if ($row['num'] > 0) {
            header("Location: Registratie.php?signup=erroruser");
            exit();
        }

        //Beveilig de wachtwoorden.
        $passwordHash = hash("sha256", $pass);

        //Bereid de INSERT statement.

        $sql = "INSERT INTO `user` (username,firstname,lastname,email,tel, password,authentication,reset) 
        VALUES (:username,:firstname,:lastname,:email,:tel,:pass,'string',0)";
        $stmt = $pdo->prepare($sql);

        //Verbind onze variables..
        $stmt->bindValue(':username', $username);
        $stmt->bindValue(':firstname', $firstname);
        $stmt->bindValue(':lastname', $lastname);
        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':tel', $tel);
        $stmt->bindValue(':pass', $passwordHash);

        //Voer de command uit en maak een account.

        $result = $stmt->execute();
        //Succes gecreerd account.
        if ($result) {
            header("Location: Registratie.php?signup=succes");
        }

    }
}


?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<form action="Registratie.php" method="POST" >

    Username:<br/>
    <input type="text" name="username" value="" required />
    <br /><br/>
    Voornaam:<br/>
    <input type="text" name="firstname" value="" required />
    <br /><br/>
    Achternaam:<br/>
    <input type="text" name="lastname" value="" required />
    <br /><br/>
    E-Mail:<br/>
    <input type="email" name="email" value="" required />
    <br /><br/>
    Mobiel Nummer:<br/>
    <input type="text" name="tel" value="" />
    <br /><br/>
    Password:<br/>
    <input type="password" name="pass" value="" required />
    <br /><br/>
    <button type="submit" class="" name="submit" >Register</button>

</form>
<form action="index.php" method="post">

    <br /><br/>
    <button type="submit" class="" name="submit" >Go back home </button>

</form>

<?php

$fullurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if (strpos($fullurl, "signup=falsevalue") == true){
    echo "<P class='error'><br>You put in symbols instead of a normal first or last name!</P>";
    exit();
}
elseif (strpos($fullurl, "signup=succes") == true){
    echo "<P class='succes'><br>Your account was created !!!</P>";
    exit();
}
elseif (strpos($fullurl, "signup=erroruser") == true){
    echo "<P class='error'><br>Username already in use !</P>";
    exit();
}
?>
</body>
</html>