<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="author" content="Bradley Oosterveen">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Slabo+27px" rel="stylesheet">
    <title>Instellingen</title>
    <script src="./frameworks/http.js"></script>
    <script src="./frameworks/cookie.js"></script>
</head>
<body>
<?php
session_start();
if (!isset($_SESSION['user_username']) || !isset($_SESSION['logged_in'])) {
    echo 'session error';
} else {
    // instellingen laden voor verschillende accoutns
    switch ($_SESSION['user_authentication']) {
        case '0': // banned
            echo <<<DOCUMENT
<span>Banned</span>
DOCUMENT;
            break;
        case '1': // gebruiker
            echo <<<DOCUMENT
<span>Welkom {$_SESSION['user_username']}</span>
DOCUMENT;
            break;
        case '2': // admin
            echo <<<DOCUMENT
<script>
    request = {
        ban: (n) => {http.request('./responses/settings.php', (p) => {response.ban(p);}, [
            ['request', 'ban'],
            ['username', n],
        ]);},
        unban: (n) => {http.request('./responses/settings.php', (p) => {response.unban(p);}, [
            ['request', 'unban'],
            ['username', n],
        ]);},
    };

    response = {
        ban: (p) => {
            switch (p['success']) {
                case 0:
                    alert(p['username'] + ' is niet gevonden.');
                    break;
                case 1:
                    alert(p['username'] + ' is nu gebanned.');
                    break;
                case 2:
                    alert(p['username'] + ' is al gebanned.');
                    break;
                case 3:
                    alert(p['username'] + ' kan niet gebanned worden.');
                    break;
                case 4:
                    alert(p['username'] + ' is niet geverifieerd.');
                    break;
            }
        },
        unban: (p) => {
            switch (p['success']) {
                case 0:
                    alert(p['username'] + ' is niet gevonden.');
                    break;
                case 1:
                    alert(p['username'] + ' is nu unbanned.');
                    break;
                case 2:
                    alert(p['username'] + ' was niet gebanned.');
                    break;
                case 4:
                    alert(p['username'] + ' is niet geverifieerd.');
                    break;
            }
        },
    }
</script>
<span>Admin: {$_SESSION['user_username']}</span><br>
<span>Ban gebruiker: </span>
<input type="text" class="ban-user">
<button type="button" onclick="request.ban(document.querySelectorAll('.ban-user')[0].value);">Ban</button><br>

<span>Unban gebruiker: </span>
<input type="text" class="unban-user">
<button type="button" onclick="request.unban(document.querySelectorAll('.unban-user')[0].value);">Unban</button><br>
DOCUMENT;
            break;
        case '3': // root
            echo <<<DOCUMENT
<span>Root: {$_SESSION['user_username']}</span>
<script>
window.addEventListener('load', main);
function main() {
    request = {
        ban: (n) => {http.request('./responses/settings.php', (p) => {response.ban(p);}, [
            ['request', 'ban'],
            ['username', n],
        ]);},
        unban: (n) => {http.request('./responses/settings.php', (p) => {response.unban(p);}, [
            ['request', 'unban'],
            ['username', n],
        ]);},
        promote: (n) => {http.request('./responses/settings.php', (p) => {response.promote(p);}, [
            ['request', 'promote'],
            ['username', n],
        ]);},
        demote: (n) => {http.request('./responses/settings.php', (p) => {response.demote(p);}, [
            ['request', 'demote'],
            ['username', n],
        ]);},
        anonymize: (b) => {http.request('./responses/settings.php', (p) => {response.anonymize(p);}, [
            ['request', 'anonymize'],
            ['anonymize', b],
        ]);},
        anonymousNamesSetting: () => {http.request('./responses/settings.php', (p) => {response.anonymousNamesSetting(p);}, [
            ['request', 'anonymous_names_setting'],
        ]);},
    };

    response = {
        ban: (p) => {
            switch (p['success']) {
                case 0:
                    alert(p['username'] + ' is niet gevonden.');
                    break;
                case 1:
                    alert(p['username'] + ' is nu gebanned.');
                    break;
                case 2:
                    alert(p['username'] + ' is al gebanned.');
                    break;
                case 3:
                    alert(p['username'] + ' kan niet gebanned worden.');
                    break;
                case 4:
                    alert(p['username'] + ' is niet geverifieerd.');
                    break;
            }
        },
        unban: (p) => {
            switch (p['success']) {
                case 0:
                    alert(p['username'] + ' is niet gevonden.');
                    break;
                case 1:
                    alert(p['username'] + ' is nu unbanned.');
                    break;
                case 2:
                    alert(p['username'] + ' was niet gebanned.');
                    break;
                case 4:
                    alert(p['username'] + ' is niet geverifieerd.');
                    break;
            }
        },
        promote: (p) => {
            switch (p['success']) {
                case 0:
                    alert(p['username'] + ' is niet gevonden.');
                    break;
                case 1:
                    alert(p['username'] + ' is nu admin.');
                    break;
                case 2:
                    alert(p['username'] + ' was al admin.');
                    break;
                case 3:
                    alert(p['username'] + ' is gebanned en kan niet promoveren.');
                    break;
                case 4:
                    alert(p['username'] + ' is niet geverifieerd.');
                    break;
            }
        },
        demote: (p) => {
            switch (p['success']) {
                case 0:
                    alert(p['username'] + ' is niet gevonden.');
                    break;
                case 1:
                    alert(p['username'] + ' is nu demoted.');
                    break;
                case 2:
                    alert(p['username'] + ' is geen admin.');
                    break;
                case 3:
                    alert(p['username'] + ' kan niet demoted worden.');
                    break;
                case 4:
                    alert(p['username'] + ' is niet geverifieerd.');
                    break;
            }
        },
        anonymize: (p) => {
            console.log(p);
            switch (p['success']) {
                case 0:
                    alert('Handeling is niet gelukt.');
                    break;
                case 1:
                    alert('Namen zijn nu verborgen.');
                    break;
                case 2:
                    alert('Namen zijn nu zichtbaar.');
                    break;
            }
        },
        anonymousNamesSetting: function(p) {
            switch (p['success']) {
                case 0:
                    console.log(p);
                    alert('ERROR see log');
                    break;
                case 1:
                    document.querySelectorAll('.anonymize-names')[0].checked = parseInt(p['anonymous_names_setting']);
                    break;
                case 2:
                    alert('Setting niet gevonden.');
                    break;
            }
        }
    }
    request.anonymousNamesSetting();
}
</script>
<span>Root: {$_SESSION['user_username']}</span><br>
<span>Ban gebruiker: </span>
<input type="text" class="ban-user">
<button type="button" onclick="request.ban(document.querySelectorAll('.ban-user')[0].value);">Ban</button><br>

<span>Unban gebruiker: </span>
<input type="text" class="unban-user">
<button type="button" onclick="request.unban(document.querySelectorAll('.unban-user')[0].value);">Unban</button><br>

<span>Promoveer gebruiker: </span>
<input type="text" class="promote-user">
<button type="button" onclick="request.promote(document.querySelectorAll('.promote-user')[0].value);">Promoveer</button><br>

<span>Demoveer gebruiker: </span>
<input type="text" class="demote-user">
<button type="button" onclick="request.demote(document.querySelectorAll('.demote-user')[0].value);">Demoveer</button><br>

<span>Alle namen anoniem: </span><input type="checkbox" class="anonymize-names" onclick="request.anonymize(this.checked == true ? 1 : 0);"></input>
DOCUMENT;
            break;
        default: // niet geverifieerd
            echo <<<DOCUMENT
<span>Niet geverifieerde gebruiker: {$_SESSION['user_username']}</span>
DOCUMENT;
            break;
    }
}
?>
</body>
</html>



